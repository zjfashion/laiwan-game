package com.java80.littlegame.service.quartz;

public class CronJobTools {
	/***
	 * 用户相关倒计时
	 * 
	 * @param userId
	 * @param cronType
	 * @param delay
	 * @param task
	 */
	public static void addCronTask(long userId, int cronType, long delay, CronTask task) {
		task.setCronType(CronTypeCost.PREFIX_USER_CRON_JOG);
		CronTaskMgr.getInstance().addCountDownJob(CronTypeCost.PREFIX_USER_CRON_JOG + userId + "_" + task.getId(),
				CronTypeCost.PREFIX_USER_CRON_JOG + userId, task);
	}

	/***
	 * 系统任务
	 * 
	 * @param cronType
	 * @param delay
	 * @param task
	 */
	public static void addSystemTask(CronTask task) {
		task.setCronType(CronTypeCost.PREFIX_SYS_CRON_JOG);
		CronTaskMgr.getInstance().addCountDownJob(CronTypeCost.PREFIX_SYS_CRON_JOG + "_" + task.getId(),
				CronTypeCost.PREFIX_SYS_CRON_JOG, task);
		/*
		 * CronTaskMgr.getInstance().addCountDownJob(CronTypeCost.
		 * PREFIX_SYS_LOOP_CRON_JOG + "_" + task.getId(),
		 * CronTypeCost.PREFIX_SYS_LOOP_CRON_JOG, task, reg);
		 */
	}
}
