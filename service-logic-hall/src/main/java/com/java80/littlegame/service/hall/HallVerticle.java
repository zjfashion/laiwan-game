package com.java80.littlegame.service.hall;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hazelcast.config.Config;
import com.java80.littlegame.common.base.BaseHandler;
import com.java80.littlegame.common.base.BaseVerticle;
import com.java80.littlegame.common.base.Runner;
import com.java80.littlegame.common.base.ServiceStatus;
import com.java80.littlegame.common.base.SystemConsts;

import io.vertx.core.VertxOptions;
import io.vertx.spi.cluster.hazelcast.HazelcastClusterManager;

public class HallVerticle extends BaseVerticle {
	final transient static Logger log = LoggerFactory.getLogger(HallVerticle.class);

	@Override
	public void start() throws Exception {
		super.start();
		log.info("service start success! {}", HallConfig.getServiceId());
	}

	@Override
	public BaseHandler getHandler() {
		return new HallHandler();
	}

	@Override
	public ServiceStatus getServiceStatus() {
		ServiceStatus ss = new ServiceStatus();
		ss.setInstanceName(HallConfig.getInstanceName());
		ss.setServiceId(HallConfig.getServiceId());
		ss.setServiceQueueName(HallConfig.getQueueName());
		ss.setServiceType(SystemConsts.SERVICE_TYPE_HALL);
		return ss;
	}

	@Override
	public boolean needPublishServiceStatus() {
		return true;
	}

	@Override
	public String queueName() {
		return HallConfig.getQueueName();
	}

	public static void main(String[] args) {
		Runner.run(HallVerticle.class,
				new VertxOptions()
						.setClusterManager(new HazelcastClusterManager(new Config(HallConfig.getInstanceName())))
						.setClustered(true));
	}
}
