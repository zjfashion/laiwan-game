package com.java80.littlegame.common.base;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPools {
	private static ExecutorService fixedThreadPool = Executors.newFixedThreadPool(BaseConfig.getThreadSize());
	static {
	}

	public static void addTask(final Task task) {
		fixedThreadPool.execute(new Runnable() {
			public void run() {
				task.work();
			}
		});
	}
}
