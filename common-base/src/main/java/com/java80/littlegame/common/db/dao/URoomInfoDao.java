package com.java80.littlegame.common.db.dao;

import com.java80.littlegame.common.db.dao.base.BaseUserDaoInterface;
import com.java80.littlegame.common.db.entity.URoomInfo;

public interface URoomInfoDao extends BaseUserDaoInterface<URoomInfo> {
	public void deletByUserId(long userId);
}
