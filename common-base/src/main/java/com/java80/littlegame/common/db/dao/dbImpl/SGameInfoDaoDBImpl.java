package com.java80.littlegame.common.db.dao.dbImpl;

import java.util.Arrays;
import java.util.List;

import com.java80.littlegame.common.db.dao.SGameInfoDao;
import com.java80.littlegame.common.db.dao.base.BaseSysDao;
import com.java80.littlegame.common.db.entity.SGameInfo;

public class SGameInfoDaoDBImpl extends BaseSysDao<SGameInfo> implements SGameInfoDao {

	@Override
	public List<SGameInfo> getAll() {
		List<SGameInfo> find = super.find("select * from s_game_info", Arrays.asList(), SGameInfo.class);
		return find;
	}

}
